
HumanSu.png
size: 512, 256
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/dark_eye
  rotate: true
  xy: 484, 30
  size: 40, 18
  orig: 44, 20
  offset: 2, 1
  index: -1
dark/face
  rotate: false
  xy: 2, 4
  size: 70, 70
  orig: 70, 70
  offset: 0, 0
  index: -1
dark/hat_ma3
  rotate: false
  xy: 367, 76
  size: 95, 56
  orig: 95, 56
  offset: 0, 0
  index: -1
ef/fog
  rotate: false
  xy: 2, 76
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
ef/green/cross_explotion_0000_1
  rotate: false
  xy: 494, 15
  size: 13, 13
  orig: 37, 49
  offset: 12, 18
  index: -1
ef/green/cross_explotion_0001_2
  rotate: false
  xy: 464, 72
  size: 24, 36
  orig: 37, 49
  offset: 6, 6
  index: -1
ef/green/cross_explotion_0002_3
  rotate: false
  xy: 233, 16
  size: 33, 45
  orig: 37, 49
  offset: 2, 2
  index: -1
ef/green/cross_explotion_0003_4
  rotate: false
  xy: 323, 27
  size: 35, 47
  orig: 37, 49
  offset: 1, 1
  index: -1
ef/green/cross_explotion_0004_5
  rotate: false
  xy: 196, 14
  size: 35, 47
  orig: 37, 49
  offset: 1, 1
  index: -1
glow_su
  rotate: false
  xy: 233, 2
  size: 12, 12
  orig: 14, 14
  offset: 1, 1
  index: -1
ra2/arm_r
  rotate: false
  xy: 358, 13
  size: 31, 60
  orig: 31, 60
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 74, 34
  size: 120, 27
  orig: 122, 29
  offset: 1, 1
  index: -1
su1/arm_1_l
  rotate: false
  xy: 457, 34
  size: 25, 36
  orig: 25, 36
  offset: 0, 0
  index: -1
su2/arm_1_l
  rotate: false
  xy: 457, 34
  size: 25, 36
  orig: 25, 36
  offset: 0, 0
  index: -1
su3/arm_1_l
  rotate: false
  xy: 457, 34
  size: 25, 36
  orig: 25, 36
  offset: 0, 0
  index: -1
su1/arm_2_l
  rotate: false
  xy: 427, 34
  size: 28, 40
  orig: 28, 40
  offset: 0, 0
  index: -1
su2/arm_2_l
  rotate: false
  xy: 427, 34
  size: 28, 40
  orig: 28, 40
  offset: 0, 0
  index: -1
su3/arm_2_l
  rotate: false
  xy: 427, 34
  size: 28, 40
  orig: 28, 40
  offset: 0, 0
  index: -1
su1/body
  rotate: false
  xy: 270, 9
  size: 51, 65
  orig: 51, 65
  offset: 0, 0
  index: -1
su2/body
  rotate: false
  xy: 270, 9
  size: 51, 65
  orig: 51, 65
  offset: 0, 0
  index: -1
su3/body
  rotate: false
  xy: 270, 9
  size: 51, 65
  orig: 51, 65
  offset: 0, 0
  index: -1
su1/face
  rotate: true
  xy: 187, 63
  size: 69, 81
  orig: 69, 81
  offset: 0, 0
  index: -1
su2/face
  rotate: true
  xy: 187, 63
  size: 69, 81
  orig: 69, 81
  offset: 0, 0
  index: -1
su1/foot_l
  rotate: false
  xy: 466, 12
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su2/foot_l
  rotate: false
  xy: 466, 12
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su3/foot_l
  rotate: false
  xy: 466, 12
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su1/foot_r
  rotate: false
  xy: 323, 9
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su2/foot_r
  rotate: false
  xy: 323, 9
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su3/foot_r
  rotate: false
  xy: 323, 9
  size: 26, 16
  orig: 26, 16
  offset: 0, 0
  index: -1
su1/leg_l
  rotate: true
  xy: 427, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su2/leg_l
  rotate: true
  xy: 427, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su3/leg_l
  rotate: true
  xy: 427, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su1/leg_r
  rotate: true
  xy: 143, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su2/leg_r
  rotate: true
  xy: 143, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su3/leg_r
  rotate: true
  xy: 143, 6
  size: 26, 37
  orig: 26, 37
  offset: 0, 0
  index: -1
su1/staff
  rotate: false
  xy: 74, 7
  size: 67, 25
  orig: 67, 25
  offset: 0, 0
  index: -1
su2/staff
  rotate: false
  xy: 74, 7
  size: 67, 25
  orig: 67, 25
  offset: 0, 0
  index: -1
su3/staff
  rotate: false
  xy: 74, 7
  size: 67, 25
  orig: 67, 25
  offset: 0, 0
  index: -1
su2/book
  rotate: false
  xy: 384, 5
  size: 41, 29
  orig: 41, 29
  offset: 0, 0
  index: -1
su3/book
  rotate: false
  xy: 384, 5
  size: 41, 29
  orig: 41, 29
  offset: 0, 0
  index: -1
su3/face
  rotate: true
  xy: 104, 63
  size: 69, 81
  orig: 69, 81
  offset: 0, 0
  index: -1
su3/hat
  rotate: false
  xy: 270, 76
  size: 95, 56
  orig: 95, 56
  offset: 0, 0
  index: -1
