
GnollSu.png
size: 512, 256
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye_dark
  rotate: false
  xy: 154, 240
  size: 16, 11
  orig: 20, 12
  offset: 2, 1
  index: -1
dark/face
  rotate: true
  xy: 81, 171
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ef/fog
  rotate: true
  xy: 143, 69
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
glow_su
  rotate: false
  xy: 172, 239
  size: 12, 12
  orig: 14, 14
  offset: 1, 1
  index: -1
ma1/arm_1_l
  rotate: false
  xy: 234, 40
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma2/arm_1_l
  rotate: false
  xy: 234, 40
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma3/arm_1_l
  rotate: false
  xy: 234, 40
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma1/arm_1_r
  rotate: true
  xy: 143, 12
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma2/arm_1_r
  rotate: true
  xy: 143, 12
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma3/arm_1_r
  rotate: true
  xy: 143, 12
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma1/arm_2_l
  rotate: false
  xy: 243, 192
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma2/arm_2_l
  rotate: false
  xy: 243, 192
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma3/arm_2_l
  rotate: false
  xy: 243, 192
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma1/arm_2_r
  rotate: false
  xy: 234, 104
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma2/arm_2_r
  rotate: false
  xy: 234, 104
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma3/arm_2_r
  rotate: false
  xy: 234, 104
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma1/body
  rotate: false
  xy: 2, 121
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma2/body
  rotate: false
  xy: 2, 121
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma3/body
  rotate: false
  xy: 2, 121
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma1/face
  rotate: true
  xy: 70, 39
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma2/face
  rotate: true
  xy: 70, 39
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma3/face
  rotate: true
  xy: 70, 39
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma1/foot_l
  rotate: true
  xy: 107, 2
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma2/foot_l
  rotate: true
  xy: 107, 2
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma3/foot_l
  rotate: true
  xy: 107, 2
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma1/foot_r
  rotate: false
  xy: 70, 15
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma2/foot_r
  rotate: false
  xy: 70, 15
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma3/foot_r
  rotate: false
  xy: 70, 15
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma1/leg_l
  rotate: false
  xy: 108, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma2/leg_l
  rotate: false
  xy: 108, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma3/leg_l
  rotate: false
  xy: 108, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma1/leg_r
  rotate: false
  xy: 81, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma2/leg_r
  rotate: false
  xy: 81, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma3/leg_r
  rotate: false
  xy: 81, 125
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 205, 49
  size: 120, 27
  orig: 122, 29
  offset: 1, 1
  index: -1
su1/wepon
  rotate: false
  xy: 205, 6
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
su2/wepon
  rotate: false
  xy: 205, 6
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
su3/wepon
  rotate: false
  xy: 205, 6
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
su2/body_deco
  rotate: false
  xy: 2, 14
  size: 66, 105
  orig: 66, 105
  offset: 0, 0
  index: -1
su3/body_deco
  rotate: false
  xy: 2, 14
  size: 66, 105
  orig: 66, 105
  offset: 0, 0
  index: -1
su3/manteau
  rotate: true
  xy: 154, 171
  size: 66, 88
  orig: 66, 88
  offset: 0, 0
  index: -1
