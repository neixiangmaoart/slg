
GnollMa.png
size: 512, 256
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye_dark
  rotate: false
  xy: 154, 242
  size: 16, 11
  orig: 20, 12
  offset: 2, 1
  index: -1
dark/face
  rotate: true
  xy: 81, 173
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ef/fog
  rotate: true
  xy: 143, 71
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
ef/magnamon_00
  rotate: false
  xy: 81, 125
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
ef/magnamon_01
  rotate: true
  xy: 70, 6
  size: 33, 37
  orig: 78, 74
  offset: 19, 15
  index: -1
ef/magnamon_02
  rotate: false
  xy: 390, 108
  size: 40, 46
  orig: 78, 74
  offset: 15, 11
  index: -1
ef/magnamon_03
  rotate: false
  xy: 344, 202
  size: 45, 51
  orig: 78, 74
  offset: 13, 9
  index: -1
ef/magnamon_04
  rotate: false
  xy: 295, 23
  size: 48, 54
  orig: 78, 74
  offset: 11, 8
  index: -1
ef/magnamon_05
  rotate: false
  xy: 292, 197
  size: 50, 56
  orig: 78, 74
  offset: 10, 7
  index: -1
ef/magnamon_06
  rotate: false
  xy: 290, 79
  size: 51, 55
  orig: 78, 74
  offset: 9, 7
  index: -1
ef/magnamon_07
  rotate: false
  xy: 281, 136
  size: 53, 53
  orig: 78, 74
  offset: 8, 10
  index: -1
ef/magnamon_08
  rotate: false
  xy: 234, 50
  size: 54, 54
  orig: 78, 74
  offset: 8, 9
  index: -1
ef/magnamon_09
  rotate: false
  xy: 343, 83
  size: 45, 55
  orig: 78, 74
  offset: 17, 9
  index: -1
ef/magnamon_10
  rotate: false
  xy: 336, 140
  size: 45, 55
  orig: 78, 74
  offset: 17, 9
  index: -1
ef/magnamon_11
  rotate: false
  xy: 345, 31
  size: 45, 50
  orig: 78, 74
  offset: 17, 14
  index: -1
ef/magnamon_12
  rotate: false
  xy: 383, 156
  size: 44, 44
  orig: 78, 74
  offset: 17, 14
  index: -1
ef/magnamon_13
  rotate: false
  xy: 391, 212
  size: 41, 41
  orig: 78, 74
  offset: 17, 17
  index: -1
ef/magnamon_14
  rotate: true
  xy: 295, 2
  size: 19, 31
  orig: 78, 74
  offset: 39, 22
  index: -1
golw_ma
  rotate: false
  xy: 172, 241
  size: 12, 12
  orig: 14, 14
  offset: 1, 1
  index: -1
ma1/arm_1_l
  rotate: false
  xy: 243, 191
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma2/arm_1_l
  rotate: false
  xy: 243, 191
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma3/arm_1_l
  rotate: false
  xy: 243, 191
  size: 47, 62
  orig: 47, 62
  offset: 0, 0
  index: -1
ma1/arm_1_r
  rotate: true
  xy: 143, 14
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma2/arm_1_r
  rotate: true
  xy: 143, 14
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma3/arm_1_r
  rotate: true
  xy: 143, 14
  size: 55, 60
  orig: 55, 60
  offset: 0, 0
  index: -1
ma1/arm_2_l
  rotate: true
  xy: 81, 128
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma2/arm_2_l
  rotate: true
  xy: 81, 128
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma3/arm_2_l
  rotate: true
  xy: 81, 128
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
ma1/arm_2_r
  rotate: false
  xy: 234, 106
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma2/arm_2_r
  rotate: false
  xy: 234, 106
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma3/arm_2_r
  rotate: false
  xy: 234, 106
  size: 45, 65
  orig: 45, 65
  offset: 0, 0
  index: -1
ma1/body
  rotate: false
  xy: 2, 123
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma2/body
  rotate: false
  xy: 2, 123
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma3/body
  rotate: false
  xy: 2, 123
  size: 77, 130
  orig: 77, 130
  offset: 0, 0
  index: -1
ma1/face
  rotate: true
  xy: 70, 41
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma2/face
  rotate: true
  xy: 70, 41
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma3/face
  rotate: true
  xy: 70, 41
  size: 80, 71
  orig: 80, 71
  offset: 0, 0
  index: -1
ma1/foot_l
  rotate: true
  xy: 109, 4
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma2/foot_l
  rotate: true
  xy: 109, 4
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma3/foot_l
  rotate: true
  xy: 109, 4
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma1/foot_r
  rotate: false
  xy: 345, 7
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma2/foot_r
  rotate: false
  xy: 345, 7
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma3/foot_r
  rotate: false
  xy: 345, 7
  size: 35, 22
  orig: 35, 22
  offset: 0, 0
  index: -1
ma1/leg_l
  rotate: false
  xy: 392, 16
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma2/leg_l
  rotate: false
  xy: 392, 16
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma3/leg_l
  rotate: false
  xy: 392, 16
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma1/leg_r
  rotate: false
  xy: 392, 62
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma2/leg_r
  rotate: false
  xy: 392, 62
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma3/leg_r
  rotate: false
  xy: 392, 62
  size: 25, 44
  orig: 25, 44
  offset: 0, 0
  index: -1
ma1/wepon
  rotate: false
  xy: 205, 16
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
ma2/wepon
  rotate: false
  xy: 205, 16
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
ma3/wepon
  rotate: false
  xy: 205, 16
  size: 88, 32
  orig: 88, 32
  offset: 0, 0
  index: -1
ma2/body_deco
  rotate: false
  xy: 2, 16
  size: 66, 105
  orig: 66, 105
  offset: 0, 0
  index: -1
ma3/body_deco
  rotate: false
  xy: 2, 16
  size: 66, 105
  orig: 66, 105
  offset: 0, 0
  index: -1
ma3/manteau
  rotate: true
  xy: 154, 173
  size: 66, 88
  orig: 66, 88
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 205, 51
  size: 120, 27
  orig: 122, 29
  offset: 1, 1
  index: -1
