
Dalph.png
size: 1024, 1024
format: RGBA8888
filter: Linear, Linear
repeat: none
e1/arm_l
  rotate: false
  xy: 955, 904
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e2/arm_l
  rotate: false
  xy: 955, 904
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e1/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e2/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e3/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e4/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e5/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e6/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p1/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p2/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p3/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p4/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p5/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
p6/body
  rotate: true
  xy: 907, 765
  size: 100, 115
  orig: 100, 115
  offset: 0, 0
  index: -1
e1/head
  rotate: true
  xy: 401, 891
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
e2/head
  rotate: true
  xy: 401, 891
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
e3/head
  rotate: true
  xy: 401, 891
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
e4/head
  rotate: true
  xy: 401, 891
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
e1/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e2/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e3/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e4/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e5/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e6/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p1/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p2/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p3/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p4/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p5/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
p6/leg_l
  rotate: false
  xy: 732, 257
  size: 54, 56
  orig: 54, 56
  offset: 0, 0
  index: -1
e1/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e2/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e3/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e4/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e5/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e6/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p1/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p2/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p3/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p4/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p5/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p6/leg_r
  rotate: false
  xy: 719, 696
  size: 55, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e1/shield
  rotate: false
  xy: 279, 667
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e2/shield
  rotate: false
  xy: 279, 667
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e1/wepon
  rotate: false
  xy: 400, 667
  size: 125, 101
  orig: 136, 101
  offset: 11, 0
  index: -1
e2/wepon
  rotate: false
  xy: 400, 667
  size: 125, 101
  orig: 136, 101
  offset: 11, 0
  index: -1
p1/wepon
  rotate: false
  xy: 400, 667
  size: 125, 101
  orig: 136, 101
  offset: 11, 0
  index: -1
p2/wepon
  rotate: false
  xy: 400, 667
  size: 125, 101
  orig: 136, 101
  offset: 11, 0
  index: -1
e2/arm_r
  rotate: true
  xy: 389, 79
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e2/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
e3/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
e4/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
e5/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
e6/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
p2/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
p3/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
p4/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
p5/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
p6/belt
  rotate: false
  xy: 2, 20
  size: 92, 27
  orig: 92, 27
  offset: 0, 0
  index: -1
e2/rope
  rotate: true
  xy: 669, 683
  size: 78, 48
  orig: 78, 48
  offset: 0, 0
  index: -1
p2/rope
  rotate: true
  xy: 669, 683
  size: 78, 48
  orig: 78, 48
  offset: 0, 0
  index: -1
e2/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
p2/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
p3/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
p4/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
p5/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
p6/shadow_shield
  rotate: false
  xy: 380, 11
  size: 38, 62
  orig: 38, 63
  offset: 0, 1
  index: -1
e2/shadow_wepon
  rotate: false
  xy: 771, 548
  size: 43, 19
  orig: 43, 19
  offset: 0, 0
  index: -1
p2/shadow_wepon
  rotate: false
  xy: 771, 548
  size: 43, 19
  orig: 43, 19
  offset: 0, 0
  index: -1
e2/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
p2/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
p3/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
p4/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
p5/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
p6/shield_deco
  rotate: false
  xy: 592, 24
  size: 54, 52
  orig: 54, 52
  offset: 0, 0
  index: -1
e3/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e4/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e5/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e6/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p1/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p2/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p3/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p4/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p5/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p6/arm_l
  rotate: true
  xy: 260, 10
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e3/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e4/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e5/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e6/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p2/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p3/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p4/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p5/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p6/arm_r
  rotate: true
  xy: 509, 78
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
e3/shield
  rotate: false
  xy: 176, 483
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e4/shield
  rotate: false
  xy: 176, 483
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e3/shoulder_l
  rotate: false
  xy: 420, 20
  size: 52, 57
  orig: 55, 57
  offset: 3, 0
  index: -1
e3/shoulder_r
  rotate: true
  xy: 474, 24
  size: 52, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
e3/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
e4/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
e5/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
p3/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
p4/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
p5/wepon
  rotate: false
  xy: 247, 172
  size: 140, 86
  orig: 140, 86
  offset: 0, 0
  index: -1
e4/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
e5/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
e6/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
p4/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
p5/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
p6/belt_deco
  rotate: false
  xy: 683, 519
  size: 62, 42
  orig: 62, 42
  offset: 0, 0
  index: -1
e4/shoulder_l
  rotate: false
  xy: 958, 697
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
e5/shoulder_l
  rotate: false
  xy: 958, 697
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
e6/shoulder_l
  rotate: false
  xy: 958, 697
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
e4/shoulder_r
  rotate: false
  xy: 709, 451
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
e5/shoulder_r
  rotate: false
  xy: 709, 451
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
e6/shoulder_r
  rotate: false
  xy: 709, 451
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
e4/wepon_back
  rotate: false
  xy: 389, 144
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
e5/wepon_back
  rotate: false
  xy: 389, 144
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
e6/wepon_back
  rotate: false
  xy: 389, 144
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
e5/body_deco
  rotate: false
  xy: 2, 2
  size: 98, 16
  orig: 98, 16
  offset: 0, 0
  index: -1
e6/body_deco
  rotate: false
  xy: 2, 2
  size: 98, 16
  orig: 98, 16
  offset: 0, 0
  index: -1
p5/body_deco
  rotate: false
  xy: 2, 2
  size: 98, 16
  orig: 98, 16
  offset: 0, 0
  index: -1
p6/body_deco
  rotate: false
  xy: 2, 2
  size: 98, 16
  orig: 98, 16
  offset: 0, 0
  index: -1
e5/head
  rotate: false
  xy: 2, 610
  size: 156, 212
  orig: 156, 212
  offset: 0, 0
  index: -1
e5/shield
  rotate: false
  xy: 297, 502
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e5/wepon_deco
  rotate: true
  xy: 748, 659
  size: 35, 25
  orig: 35, 25
  offset: 0, 0
  index: -1
e6/wepon_deco
  rotate: true
  xy: 748, 659
  size: 35, 25
  orig: 35, 25
  offset: 0, 0
  index: -1
p5/wepon_deco
  rotate: true
  xy: 748, 659
  size: 35, 25
  orig: 35, 25
  offset: 0, 0
  index: -1
p6/wepon_deco
  rotate: true
  xy: 748, 659
  size: 35, 25
  orig: 35, 25
  offset: 0, 0
  index: -1
e6/arm_r_decp
  rotate: true
  xy: 732, 214
  size: 41, 56
  orig: 41, 56
  offset: 0, 0
  index: -1
p6/arm_r_decp
  rotate: true
  xy: 732, 214
  size: 41, 56
  orig: 41, 56
  offset: 0, 0
  index: -1
e6/head
  rotate: true
  xy: 187, 866
  size: 156, 212
  orig: 156, 212
  offset: 0, 0
  index: -1
e6/shield_deco_3
  rotate: false
  xy: 247, 260
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
e6/wepon
  rotate: false
  xy: 527, 670
  size: 140, 91
  orig: 140, 91
  offset: 0, 0
  index: -1
p6/wepon
  rotate: false
  xy: 527, 670
  size: 140, 91
  orig: 140, 91
  offset: 0, 0
  index: -1
e6/wepon_p6
  rotate: true
  xy: 638, 338
  size: 101, 94
  orig: 101, 94
  offset: 0, 0
  index: -1
p6/wepon_p6
  rotate: true
  xy: 638, 338
  size: 101, 94
  orig: 101, 94
  offset: 0, 0
  index: -1
ef
  rotate: false
  xy: 515, 143
  size: 118, 126
  orig: 118, 137
  offset: 0, 11
  index: -1
ef/1
  rotate: false
  xy: 637, 441
  size: 70, 70
  orig: 94, 94
  offset: 12, 12
  index: -1
ef/2
  rotate: false
  xy: 551, 427
  size: 84, 84
  orig: 94, 94
  offset: 5, 5
  index: -1
ef/3
  rotate: false
  xy: 638, 244
  size: 92, 92
  orig: 94, 94
  offset: 1, 1
  index: -1
ef/4
  rotate: false
  xy: 635, 150
  size: 92, 92
  orig: 94, 94
  offset: 1, 1
  index: -1
ef/5
  rotate: false
  xy: 776, 659
  size: 90, 90
  orig: 94, 94
  offset: 2, 2
  index: -1
ef/6
  rotate: false
  xy: 748, 569
  size: 88, 88
  orig: 94, 94
  offset: 3, 3
  index: -1
ef/7
  rotate: false
  xy: 868, 661
  size: 88, 88
  orig: 94, 94
  offset: 3, 3
  index: -1
ef/8
  rotate: false
  xy: 927, 573
  size: 86, 86
  orig: 94, 94
  offset: 4, 4
  index: -1
ef/9
  rotate: false
  xy: 838, 570
  size: 87, 87
  orig: 94, 94
  offset: 4, 3
  index: -1
ef/fog
  rotate: false
  xy: 176, 425
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
ef/impact_0000_6
  rotate: false
  xy: 96, 24
  size: 23, 23
  orig: 196, 216
  offset: 2, 191
  index: -1
ef/impact_0001_5
  rotate: true
  xy: 187, 832
  size: 32, 208
  orig: 196, 216
  offset: 6, 2
  index: -1
ef/impact_0002_4
  rotate: false
  xy: 2, 824
  size: 183, 198
  orig: 196, 216
  offset: 11, 4
  index: -1
ef/impact_0003_3
  rotate: false
  xy: 2, 425
  size: 172, 183
  orig: 196, 216
  offset: 17, 11
  index: -1
ef/impact_0004_2
  rotate: false
  xy: 489, 271
  size: 147, 154
  orig: 196, 216
  offset: 29, 31
  index: -1
ef/impact_0005_1
  rotate: false
  xy: 135, 133
  size: 109, 108
  orig: 196, 216
  offset: 51, 53
  index: -1
ef/impact_2_0000_8
  rotate: true
  xy: 724, 755
  size: 129, 50
  orig: 182, 172
  offset: 7, 121
  index: -1
ef/impact_2_0001_7
  rotate: false
  xy: 297, 425
  size: 130, 75
  orig: 182, 172
  offset: 7, 95
  index: -1
ef/impact_2_0002_6
  rotate: true
  xy: 135, 243
  size: 180, 110
  orig: 182, 172
  offset: 1, 59
  index: -1
ef/impact_2_0003_5
  rotate: false
  xy: 776, 906
  size: 177, 116
  orig: 182, 172
  offset: 3, 50
  index: -1
ef/impact_2_0004_4
  rotate: true
  xy: 160, 648
  size: 174, 117
  orig: 182, 172
  offset: 4, 43
  index: -1
ef/impact_2_0005_3
  rotate: false
  xy: 608, 886
  size: 166, 136
  orig: 182, 172
  offset: 9, 19
  index: -1
ef/impact_2_0006_2
  rotate: true
  xy: 539, 513
  size: 155, 142
  orig: 182, 172
  offset: 16, 2
  index: -1
ef/impact_2_0007_1
  rotate: false
  xy: 776, 751
  size: 129, 114
  orig: 182, 172
  offset: 32, 1
  index: -1
eye_enemy
  rotate: false
  xy: 958, 680
  size: 61, 15
  orig: 62, 15
  offset: 0, 0
  index: -1
mouth_At
  rotate: true
  xy: 940, 876
  size: 28, 13
  orig: 28, 13
  offset: 0, 0
  index: -1
p1/arm_r
  rotate: false
  xy: 683, 563
  size: 63, 118
  orig: 63, 118
  offset: 0, 0
  index: -1
p1/head
  rotate: false
  xy: 2, 218
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
p2/head
  rotate: false
  xy: 2, 218
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
p3/head
  rotate: false
  xy: 2, 218
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
p4/head
  rotate: false
  xy: 2, 218
  size: 131, 205
  orig: 131, 205
  offset: 0, 0
  index: -1
p1/shield
  rotate: false
  xy: 368, 260
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p2/shield
  rotate: false
  xy: 368, 260
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p3/shield
  rotate: false
  xy: 368, 260
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p4/shield
  rotate: false
  xy: 368, 260
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p3/shield_deco_2
  rotate: true
  xy: 401, 770
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p4/shield_deco_2
  rotate: true
  xy: 401, 770
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p5/shield_deco_2
  rotate: true
  xy: 401, 770
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p6/shield_deco_2
  rotate: true
  xy: 401, 770
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p3/shoulder_l
  rotate: true
  xy: 533, 24
  size: 52, 57
  orig: 55, 57
  offset: 3, 0
  index: -1
p3/shoulder_r
  rotate: false
  xy: 729, 155
  size: 52, 57
  orig: 55, 57
  offset: 0, 0
  index: -1
p4/shoulder_l
  rotate: false
  xy: 734, 383
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
p5/shoulder_l
  rotate: false
  xy: 734, 383
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
p6/shoulder_l
  rotate: false
  xy: 734, 383
  size: 62, 66
  orig: 65, 66
  offset: 3, 0
  index: -1
p4/shoulder_r
  rotate: false
  xy: 734, 315
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
p5/shoulder_r
  rotate: false
  xy: 734, 315
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
p6/shoulder_r
  rotate: false
  xy: 734, 315
  size: 62, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
p4/wepon_back
  rotate: false
  xy: 134, 17
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
p5/wepon_back
  rotate: false
  xy: 134, 17
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
p6/wepon_back
  rotate: false
  xy: 134, 17
  size: 124, 114
  orig: 124, 114
  offset: 0, 0
  index: -1
p5/head
  rotate: false
  xy: 2, 49
  size: 130, 167
  orig: 130, 167
  offset: 0, 0
  index: -1
p6/head
  rotate: false
  xy: 2, 49
  size: 130, 167
  orig: 130, 167
  offset: 0, 0
  index: -1
p5/helmet
  rotate: false
  xy: 566, 763
  size: 156, 121
  orig: 156, 121
  offset: 0, 0
  index: -1
p6/helmet
  rotate: false
  xy: 566, 763
  size: 156, 121
  orig: 156, 121
  offset: 0, 0
  index: -1
p5/shield
  rotate: false
  xy: 418, 502
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p6/shield
  rotate: false
  xy: 418, 502
  size: 119, 163
  orig: 119, 163
  offset: 0, 0
  index: -1
p6/head_deco
  rotate: false
  xy: 429, 427
  size: 120, 73
  orig: 120, 73
  offset: 0, 0
  index: -1
p6/shield_deco_3
  rotate: true
  xy: 260, 75
  size: 95, 127
  orig: 95, 127
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 776, 867
  size: 162, 37
  orig: 164, 39
  offset: 1, 1
  index: -1
wepon_hand
  rotate: true
  xy: 747, 519
  size: 42, 22
  orig: 53, 22
  offset: 0, 0
  index: -1
