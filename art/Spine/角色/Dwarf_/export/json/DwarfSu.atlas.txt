
DwarfSu.png
size: 512, 256
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye_dark
  rotate: false
  xy: 2, 8
  size: 47, 25
  orig: 51, 29
  offset: 2, 2
  index: -1
dark/face
  rotate: false
  xy: 2, 35
  size: 70, 91
  orig: 71, 91
  offset: 1, 0
  index: -1
dark/hat
  rotate: false
  xy: 368, 198
  size: 95, 56
  orig: 95, 56
  offset: 0, 0
  index: -1
ef
  rotate: false
  xy: 2, 128
  size: 118, 126
  orig: 118, 137
  offset: 0, 11
  index: -1
ef/cross_explotion_0000_1
  rotate: false
  xy: 51, 20
  size: 13, 13
  orig: 37, 49
  offset: 12, 18
  index: -1
ef/cross_explotion_0001_2
  rotate: false
  xy: 378, 123
  size: 24, 36
  orig: 37, 49
  offset: 6, 6
  index: -1
ef/cross_explotion_0002_3
  rotate: true
  xy: 465, 221
  size: 33, 45
  orig: 37, 49
  offset: 2, 2
  index: -1
ef/cross_explotion_0003_4
  rotate: false
  xy: 132, 2
  size: 35, 47
  orig: 37, 49
  offset: 1, 1
  index: -1
ef/cross_explotion_0004_5
  rotate: false
  xy: 266, 112
  size: 35, 47
  orig: 37, 49
  offset: 1, 1
  index: -1
ef/fog
  rotate: false
  xy: 266, 198
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
glow_su
  rotate: false
  xy: 432, 147
  size: 12, 12
  orig: 14, 14
  offset: 1, 1
  index: -1
ma1/arm_1_l
  rotate: false
  xy: 303, 118
  size: 34, 41
  orig: 34, 41
  offset: 0, 0
  index: -1
ma2/arm_1_l
  rotate: false
  xy: 303, 118
  size: 34, 41
  orig: 34, 41
  offset: 0, 0
  index: -1
ma3/arm_1_l
  rotate: false
  xy: 303, 118
  size: 34, 41
  orig: 34, 41
  offset: 0, 0
  index: -1
ma1/arm_2_l
  rotate: false
  xy: 339, 122
  size: 37, 37
  orig: 37, 37
  offset: 0, 0
  index: -1
ma2/arm_2_l
  rotate: false
  xy: 339, 122
  size: 37, 37
  orig: 37, 37
  offset: 0, 0
  index: -1
ma3/arm_2_l
  rotate: false
  xy: 339, 122
  size: 37, 37
  orig: 37, 37
  offset: 0, 0
  index: -1
ma1/arm_r
  rotate: false
  xy: 169, 8
  size: 41, 59
  orig: 41, 59
  offset: 0, 0
  index: -1
ma2/arm_r
  rotate: false
  xy: 169, 8
  size: 41, 59
  orig: 41, 59
  offset: 0, 0
  index: -1
ma3/arm_r
  rotate: false
  xy: 169, 8
  size: 41, 59
  orig: 41, 59
  offset: 0, 0
  index: -1
ma1/body
  rotate: false
  xy: 206, 104
  size: 58, 57
  orig: 58, 57
  offset: 0, 0
  index: -1
ma2/body
  rotate: false
  xy: 206, 104
  size: 58, 57
  orig: 58, 57
  offset: 0, 0
  index: -1
ma3/body
  rotate: false
  xy: 206, 104
  size: 58, 57
  orig: 58, 57
  offset: 0, 0
  index: -1
ma1/face
  rotate: false
  xy: 122, 163
  size: 70, 91
  orig: 71, 91
  offset: 1, 0
  index: -1
ma2/face
  rotate: false
  xy: 122, 163
  size: 70, 91
  orig: 71, 91
  offset: 1, 0
  index: -1
ma1/foot_l
  rotate: true
  xy: 51, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma2/foot_l
  rotate: true
  xy: 51, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma3/foot_l
  rotate: true
  xy: 51, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma1/foot_r
  rotate: true
  xy: 61, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma2/foot_r
  rotate: true
  xy: 61, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma3/foot_r
  rotate: true
  xy: 61, 2
  size: 16, 8
  orig: 16, 8
  offset: 0, 0
  index: -1
ma1/leg_l
  rotate: false
  xy: 404, 135
  size: 26, 24
  orig: 26, 24
  offset: 0, 0
  index: -1
ma2/leg_l
  rotate: false
  xy: 404, 135
  size: 26, 24
  orig: 26, 24
  offset: 0, 0
  index: -1
ma3/leg_l
  rotate: false
  xy: 404, 135
  size: 26, 24
  orig: 26, 24
  offset: 0, 0
  index: -1
ma1/leg_r
  rotate: false
  xy: 465, 196
  size: 26, 23
  orig: 26, 23
  offset: 0, 0
  index: -1
ma2/leg_r
  rotate: false
  xy: 465, 196
  size: 26, 23
  orig: 26, 23
  offset: 0, 0
  index: -1
ma3/leg_r
  rotate: false
  xy: 465, 196
  size: 26, 23
  orig: 26, 23
  offset: 0, 0
  index: -1
ma1/wepon
  rotate: true
  xy: 169, 69
  size: 71, 29
  orig: 71, 29
  offset: 0, 0
  index: -1
ma2/wepon
  rotate: true
  xy: 169, 69
  size: 71, 29
  orig: 71, 29
  offset: 0, 0
  index: -1
ma3/wepon
  rotate: true
  xy: 169, 69
  size: 71, 29
  orig: 71, 29
  offset: 0, 0
  index: -1
ma2/manteau_neck
  rotate: false
  xy: 266, 161
  size: 89, 35
  orig: 89, 35
  offset: 0, 0
  index: -1
ma3/face
  rotate: false
  xy: 194, 163
  size: 70, 91
  orig: 71, 91
  offset: 1, 0
  index: -1
ma3/manteau_neck
  rotate: false
  xy: 357, 161
  size: 89, 35
  orig: 89, 35
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 122, 142
  size: 82, 19
  orig: 82, 20
  offset: 0, 0
  index: -1
su2/manteau_neck
  rotate: true
  xy: 132, 51
  size: 89, 35
  orig: 89, 35
  offset: 0, 0
  index: -1
su3/manteau_neck
  rotate: true
  xy: 132, 51
  size: 89, 35
  orig: 89, 35
  offset: 0, 0
  index: -1
su3/hat
  rotate: true
  xy: 74, 31
  size: 95, 56
  orig: 95, 56
  offset: 0, 0
  index: -1
