
SkelletonRa.png
size: 512, 256
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye_dark
  rotate: false
  xy: 179, 160
  size: 34, 13
  orig: 35, 14
  offset: 1, 1
  index: -1
dark/face
  rotate: false
  xy: 179, 175
  size: 66, 72
  orig: 66, 72
  offset: 0, 0
  index: -1
dark/hat_ra
  rotate: true
  xy: 122, 157
  size: 90, 55
  orig: 90, 55
  offset: 0, 0
  index: -1
ef
  rotate: false
  xy: 2, 121
  size: 118, 126
  orig: 118, 137
  offset: 0, 11
  index: -1
ef/fog
  rotate: true
  xy: 90, 19
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
ef/impact_2_0002_6
  rotate: true
  xy: 310, 40
  size: 45, 27
  orig: 45, 43
  offset: 0, 15
  index: -1
ef/impact_2_0003_5
  rotate: true
  xy: 316, 172
  size: 43, 28
  orig: 45, 43
  offset: 1, 13
  index: -1
ef/impact_2_0004_4
  rotate: true
  xy: 304, 87
  size: 43, 29
  orig: 45, 43
  offset: 1, 11
  index: -1
ef/impact_2_0005_3
  rotate: true
  xy: 274, 25
  size: 41, 34
  orig: 45, 43
  offset: 2, 5
  index: -1
ef/impact_2_0006_2
  rotate: true
  xy: 298, 132
  size: 38, 36
  orig: 45, 43
  offset: 4, 0
  index: -1
ef/impact_2_0007_1
  rotate: false
  xy: 310, 9
  size: 32, 29
  orig: 45, 43
  offset: 8, 0
  index: -1
ra1/arm_1_l
  rotate: false
  xy: 339, 50
  size: 22, 35
  orig: 22, 35
  offset: 0, 0
  index: -1
ra2/arm_1_l
  rotate: false
  xy: 339, 50
  size: 22, 35
  orig: 22, 35
  offset: 0, 0
  index: -1
ra3/arm_1_l
  rotate: false
  xy: 339, 50
  size: 22, 35
  orig: 22, 35
  offset: 0, 0
  index: -1
ra1/arm_1_r
  rotate: false
  xy: 336, 134
  size: 22, 36
  orig: 22, 36
  offset: 0, 0
  index: -1
ra2/arm_1_r
  rotate: false
  xy: 336, 134
  size: 22, 36
  orig: 22, 36
  offset: 0, 0
  index: -1
ra3/arm_1_r
  rotate: false
  xy: 336, 134
  size: 22, 36
  orig: 22, 36
  offset: 0, 0
  index: -1
ra1/arm_2_l
  rotate: false
  xy: 274, 68
  size: 28, 50
  orig: 28, 50
  offset: 0, 0
  index: -1
ra2/arm_2_l
  rotate: false
  xy: 274, 68
  size: 28, 50
  orig: 28, 50
  offset: 0, 0
  index: -1
ra3/arm_2_l
  rotate: false
  xy: 274, 68
  size: 28, 50
  orig: 28, 50
  offset: 0, 0
  index: -1
ra1/arm_2_r
  rotate: false
  xy: 267, 120
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: -1
ra2/arm_2_r
  rotate: false
  xy: 267, 120
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: -1
ra3/arm_2_r
  rotate: false
  xy: 267, 120
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: -1
ra1/arrow
  rotate: false
  xy: 2, 43
  size: 86, 76
  orig: 86, 76
  offset: 0, 0
  index: -1
ra2/arrow
  rotate: false
  xy: 2, 43
  size: 86, 76
  orig: 86, 76
  offset: 0, 0
  index: -1
ra3/arrow
  rotate: false
  xy: 2, 43
  size: 86, 76
  orig: 86, 76
  offset: 0, 0
  index: -1
ra1/arrow_deco
  rotate: false
  xy: 122, 123
  size: 15, 32
  orig: 15, 32
  offset: 0, 0
  index: -1
ra2/arrow_deco
  rotate: false
  xy: 122, 123
  size: 15, 32
  orig: 15, 32
  offset: 0, 0
  index: -1
ra3/arrow_deco
  rotate: false
  xy: 122, 123
  size: 15, 32
  orig: 15, 32
  offset: 0, 0
  index: -1
ra1/body
  rotate: false
  xy: 216, 22
  size: 56, 66
  orig: 56, 66
  offset: 0, 0
  index: -1
ra2/body
  rotate: false
  xy: 216, 22
  size: 56, 66
  orig: 56, 66
  offset: 0, 0
  index: -1
ra3/body
  rotate: false
  xy: 216, 22
  size: 56, 66
  orig: 56, 66
  offset: 0, 0
  index: -1
ra1/face
  rotate: false
  xy: 148, 9
  size: 66, 72
  orig: 66, 72
  offset: 0, 0
  index: -1
ra2/face
  rotate: false
  xy: 148, 9
  size: 66, 72
  orig: 66, 72
  offset: 0, 0
  index: -1
ra1/foot_l
  rotate: false
  xy: 23, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra2/foot_l
  rotate: false
  xy: 23, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra3/foot_l
  rotate: false
  xy: 23, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra1/foot_r
  rotate: false
  xy: 2, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra2/foot_r
  rotate: false
  xy: 2, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra3/foot_r
  rotate: false
  xy: 2, 2
  size: 19, 18
  orig: 19, 18
  offset: 0, 0
  index: -1
ra1/leg_l
  rotate: false
  xy: 344, 12
  size: 18, 36
  orig: 18, 36
  offset: 0, 0
  index: -1
ra2/leg_l
  rotate: false
  xy: 344, 12
  size: 18, 36
  orig: 18, 36
  offset: 0, 0
  index: -1
ra3/leg_l
  rotate: false
  xy: 344, 12
  size: 18, 36
  orig: 18, 36
  offset: 0, 0
  index: -1
ra1/leg_r
  rotate: false
  xy: 247, 211
  size: 17, 36
  orig: 17, 36
  offset: 0, 0
  index: -1
ra2/leg_r
  rotate: false
  xy: 247, 211
  size: 17, 36
  orig: 17, 36
  offset: 0, 0
  index: -1
ra3/leg_r
  rotate: false
  xy: 247, 211
  size: 17, 36
  orig: 17, 36
  offset: 0, 0
  index: -1
ra2/arrow_back
  rotate: true
  xy: 267, 172
  size: 75, 47
  orig: 75, 47
  offset: 0, 0
  index: -1
ra3/arrow_back
  rotate: true
  xy: 267, 172
  size: 75, 47
  orig: 75, 47
  offset: 0, 0
  index: -1
ra3/face
  rotate: false
  xy: 148, 83
  size: 66, 72
  orig: 66, 72
  offset: 0, 0
  index: -1
ra3/hat
  rotate: true
  xy: 216, 90
  size: 83, 49
  orig: 83, 49
  offset: 0, 0
  index: -1
ra3/hat_Deco
  rotate: true
  xy: 335, 87
  size: 41, 25
  orig: 41, 25
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 2, 22
  size: 82, 19
  orig: 82, 20
  offset: 0, 1
  index: -1
