
GoblinSu.png
size: 512, 512
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye_dark
  rotate: false
  xy: 2, 2
  size: 125, 41
  orig: 137, 50
  offset: 5, 6
  index: -1
dark/face
  rotate: true
  xy: 243, 5
  size: 310, 197
  orig: 312, 200
  offset: 1, 2
  index: -1
ef/fog
  rotate: false
  xy: 2, 317
  size: 335, 186
  orig: 354, 199
  offset: 12, 6
  index: -1
ef/spark_1
  rotate: false
  xy: 2, 45
  size: 239, 270
  orig: 493, 385
  offset: 204, 46
  index: -1
ef/spark_5
  rotate: true
  xy: 486, 474
  size: 29, 21
  orig: 493, 385
  offset: 434, 259
  index: -1
ef/spark_6
  rotate: true
  xy: 486, 445
  size: 27, 19
  orig: 493, 385
  offset: 437, 261
  index: -1
glow_su
  rotate: false
  xy: 183, 3
  size: 40, 40
  orig: 48, 48
  offset: 4, 4
  index: -1
ma1/body
  rotate: false
  xy: 339, 331
  size: 145, 172
  orig: 147, 174
  offset: 1, 1
  index: -1
ma2/body
  rotate: false
  xy: 339, 331
  size: 145, 172
  orig: 147, 174
  offset: 1, 1
  index: -1
ma3/body
  rotate: false
  xy: 339, 331
  size: 145, 172
  orig: 147, 174
  offset: 1, 1
  index: -1
ma1/foot_l
  rotate: false
  xy: 129, 10
  size: 52, 33
  orig: 54, 35
  offset: 1, 1
  index: -1
ma2/foot_l
  rotate: false
  xy: 129, 10
  size: 52, 33
  orig: 54, 35
  offset: 1, 1
  index: -1
ma3/foot_l
  rotate: false
  xy: 129, 10
  size: 52, 33
  orig: 54, 35
  offset: 1, 1
  index: -1
ma1/foot_r
  rotate: false
  xy: 442, 22
  size: 54, 35
  orig: 56, 37
  offset: 1, 1
  index: -1
ma2/foot_r
  rotate: false
  xy: 442, 22
  size: 54, 35
  orig: 56, 37
  offset: 1, 1
  index: -1
ma3/foot_r
  rotate: false
  xy: 442, 22
  size: 54, 35
  orig: 56, 37
  offset: 1, 1
  index: -1
shadow
  rotate: true
  xy: 442, 59
  size: 270, 63
  orig: 274, 67
  offset: 2, 2
  index: -1

GoblinSu_2.png
size: 512, 512
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/hat_su3
  rotate: true
  xy: 314, 85
  size: 197, 160
  orig: 199, 162
  offset: 1, 1
  index: -1
ef/spark_4
  rotate: false
  xy: 314, 284
  size: 191, 215
  orig: 493, 385
  offset: 279, 134
  index: -1
ma1/arm_2_l
  rotate: true
  xy: 402, 18
  size: 65, 105
  orig: 67, 107
  offset: 1, 1
  index: -1
ma2/arm_2_l
  rotate: true
  xy: 402, 18
  size: 65, 105
  orig: 67, 107
  offset: 1, 1
  index: -1
ma3/arm_2_l
  rotate: true
  xy: 402, 18
  size: 65, 105
  orig: 67, 107
  offset: 1, 1
  index: -1
ma1/arm_r
  rotate: true
  xy: 228, 2
  size: 81, 172
  orig: 83, 174
  offset: 1, 1
  index: -1
ma2/arm_r
  rotate: true
  xy: 228, 2
  size: 81, 172
  orig: 83, 174
  offset: 1, 1
  index: -1
ma1/face
  rotate: false
  xy: 2, 302
  size: 310, 197
  orig: 312, 200
  offset: 1, 2
  index: -1
su2/face
  rotate: false
  xy: 2, 103
  size: 310, 197
  orig: 312, 200
  offset: 1, 2
  index: -1
su3/face
  rotate: false
  xy: 2, 103
  size: 310, 197
  orig: 312, 200
  offset: 1, 2
  index: -1
su3/neck
  rotate: false
  xy: 2, 25
  size: 224, 76
  orig: 226, 78
  offset: 1, 1
  index: -1

GoblinSu_3.png
size: 512, 512
format: RGBA8888
filter: Linear, Linear
repeat: none
ef/spark_0
  rotate: true
  xy: 454, 309
  size: 173, 54
  orig: 493, 385
  offset: 220, 150
  index: -1
ef/spark_2
  rotate: false
  xy: 2, 216
  size: 234, 266
  orig: 493, 385
  offset: 227, 72
  index: -1
ef/spark_3
  rotate: false
  xy: 238, 240
  size: 214, 242
  orig: 493, 385
  offset: 255, 105
  index: -1
ma1/arm_1_l
  rotate: false
  xy: 284, 126
  size: 72, 112
  orig: 74, 114
  offset: 1, 1
  index: -1
ma2/arm_1_l
  rotate: false
  xy: 284, 126
  size: 72, 112
  orig: 74, 114
  offset: 1, 1
  index: -1
ma3/arm_1_l
  rotate: false
  xy: 284, 126
  size: 72, 112
  orig: 74, 114
  offset: 1, 1
  index: -1
ma1/leg_l
  rotate: false
  xy: 358, 140
  size: 55, 98
  orig: 57, 100
  offset: 1, 1
  index: -1
ma2/leg_l
  rotate: false
  xy: 358, 140
  size: 55, 98
  orig: 57, 100
  offset: 1, 1
  index: -1
ma3/leg_l
  rotate: false
  xy: 358, 140
  size: 55, 98
  orig: 57, 100
  offset: 1, 1
  index: -1
ma1/leg_r
  rotate: false
  xy: 284, 26
  size: 55, 98
  orig: 58, 100
  offset: 2, 1
  index: -1
ma2/leg_r
  rotate: false
  xy: 284, 26
  size: 55, 98
  orig: 58, 100
  offset: 2, 1
  index: -1
ma3/leg_r
  rotate: false
  xy: 284, 26
  size: 55, 98
  orig: 58, 100
  offset: 2, 1
  index: -1
ma3/arm_r
  rotate: false
  xy: 201, 42
  size: 81, 172
  orig: 83, 174
  offset: 1, 1
  index: -1
su1/wepon
  rotate: false
  xy: 2, 2
  size: 173, 50
  orig: 175, 52
  offset: 1, 1
  index: -1
su2/wepon
  rotate: false
  xy: 2, 2
  size: 173, 50
  orig: 175, 52
  offset: 1, 1
  index: -1
su3/wepon
  rotate: false
  xy: 2, 2
  size: 173, 50
  orig: 175, 52
  offset: 1, 1
  index: -1
su2/hat
  rotate: false
  xy: 2, 54
  size: 197, 160
  orig: 199, 162
  offset: 1, 1
  index: -1
