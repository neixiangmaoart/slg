
WaterRa.png
size: 512, 128
format: RGBA8888
filter: Linear, Linear
repeat: none
dark/eye
  rotate: false
  xy: 219, 7
  size: 40, 14
  orig: 43, 16
  offset: 1, 1
  index: -1
dark/face
  rotate: true
  xy: 175, 23
  size: 69, 65
  orig: 69, 65
  offset: 0, 0
  index: -1
ef/fog
  rotate: false
  xy: 2, 36
  size: 100, 56
  orig: 106, 60
  offset: 4, 2
  index: -1
ef/straight_impact_0000_7
  rotate: false
  xy: 261, 15
  size: 24, 7
  orig: 98, 21
  offset: 74, 7
  index: -1
ef/straight_impact_0001_6
  rotate: false
  xy: 33, 5
  size: 25, 7
  orig: 98, 21
  offset: 72, 7
  index: -1
ef/straight_impact_0002_5
  rotate: false
  xy: 2, 3
  size: 29, 9
  orig: 98, 21
  offset: 68, 6
  index: -1
ef/straight_impact_0003_4
  rotate: false
  xy: 172, 8
  size: 45, 13
  orig: 98, 21
  offset: 51, 3
  index: -1
ef/straight_impact_0004_3
  rotate: false
  xy: 2, 14
  size: 82, 20
  orig: 98, 21
  offset: 12, 1
  index: -1
ef/straight_impact_0005_2
  rotate: false
  xy: 60, 2
  size: 14, 10
  orig: 98, 21
  offset: 6, 6
  index: -1
ef/straight_impact_0006_1
  rotate: false
  xy: 261, 5
  size: 11, 8
  orig: 98, 21
  offset: 1, 7
  index: -1
ra1/arm_1_l
  rotate: false
  xy: 303, 2
  size: 27, 29
  orig: 27, 29
  offset: 0, 0
  index: -1
ra2/arm_1_l
  rotate: false
  xy: 303, 2
  size: 27, 29
  orig: 27, 29
  offset: 0, 0
  index: -1
ra3/arm_1_l
  rotate: false
  xy: 303, 2
  size: 27, 29
  orig: 27, 29
  offset: 0, 0
  index: -1
ra1/arm_1_r
  rotate: true
  xy: 138, 2
  size: 23, 32
  orig: 23, 32
  offset: 0, 0
  index: -1
ra2/arm_1_r
  rotate: true
  xy: 138, 2
  size: 23, 32
  orig: 23, 32
  offset: 0, 0
  index: -1
ra3/arm_1_r
  rotate: true
  xy: 138, 2
  size: 23, 32
  orig: 23, 32
  offset: 0, 0
  index: -1
ra1/arm_2_l
  rotate: true
  xy: 369, 6
  size: 25, 50
  orig: 25, 50
  offset: 0, 0
  index: -1
ra2/arm_2_l
  rotate: true
  xy: 369, 6
  size: 25, 50
  orig: 25, 50
  offset: 0, 0
  index: -1
ra3/arm_2_l
  rotate: true
  xy: 369, 6
  size: 25, 50
  orig: 25, 50
  offset: 0, 0
  index: -1
ra1/arm_2_r
  rotate: true
  xy: 86, 3
  size: 22, 50
  orig: 22, 50
  offset: 0, 0
  index: -1
ra2/arm_2_r
  rotate: true
  xy: 86, 3
  size: 22, 50
  orig: 22, 50
  offset: 0, 0
  index: -1
ra3/arm_2_r
  rotate: true
  xy: 86, 3
  size: 22, 50
  orig: 22, 50
  offset: 0, 0
  index: -1
ra1/face
  rotate: false
  xy: 104, 27
  size: 69, 65
  orig: 69, 65
  offset: 0, 0
  index: -1
ra2/face
  rotate: false
  xy: 104, 27
  size: 69, 65
  orig: 69, 65
  offset: 0, 0
  index: -1
ra3/face
  rotate: false
  xy: 104, 27
  size: 69, 65
  orig: 69, 65
  offset: 0, 0
  index: -1
ra1/wepon
  rotate: false
  xy: 369, 45
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: -1
ra2/wepon
  rotate: false
  xy: 369, 45
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: -1
ra3/wepon
  rotate: false
  xy: 369, 45
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: -1
ra2/neck
  rotate: true
  xy: 332, 19
  size: 73, 35
  orig: 73, 35
  offset: 0, 0
  index: -1
ra3/neck
  rotate: true
  xy: 332, 19
  size: 73, 35
  orig: 73, 35
  offset: 0, 0
  index: -1
ra3/manteau
  rotate: false
  xy: 242, 23
  size: 88, 69
  orig: 88, 69
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 463, 10
  size: 82, 19
  orig: 82, 20
  offset: 0, 1
  index: -1
su2/body
  rotate: false
  xy: 418, 25
  size: 43, 67
  orig: 43, 67
  offset: 0, 0
  index: -1
